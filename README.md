# 《国产编程语言蓝皮书-2023》

本仓库是《国产编程语言蓝皮书-2023》（以下简称蓝皮书）编撰工作区。

蓝皮书编委会是编程语言开放论坛（PLOC）下属的专业委员会，经PLOC 2号提案授权成立，提案地址：[https://gitee.com/ploc-org/CPLOC/issues/I8QKWQ](https://gitee.com/ploc-org/CPLOC/issues/I8QKWQ)

---

### 蓝皮书编委会编委名单
柴树杉、李登淳、徐鹏飞、杨海龙、陈朝臣、丁尔男、杜天微、题叶、朱子润、贺师俊、魏永明、吴森、刘小东、赵普明、杨海玲

新增编委由编委会大会批准。

---

### 进度安排
- 2024/1/18-2024/1/31，项目公开征集。
- 2024/2/1-2024/2/7，资料归档、蓝皮书主体（项目列表）编写。
- 2024/2/8，蓝皮书发布。

---

### 会议纪要
2024/1/7，《国产编程语言蓝皮书-2023》编委会大会会议纪要：  
[https://gitee.com/ploc-org/CPLOC/blob/master/%E4%BC%9A%E8%AE%AE%E7%BA%AA%E8%A6%81/20240107.md](https://gitee.com/ploc-org/CPLOC/blob/master/%E4%BC%9A%E8%AE%AE%E7%BA%AA%E8%A6%81/20240107.md)

2024/1/14，《国产编程语言蓝皮书-2023》编委会会议纪要：  
[https://gitee.com/ploc-org/CPLOC/blob/master/%E4%BC%9A%E8%AE%AE%E7%BA%AA%E8%A6%81/20240114.md](2024/1/7，《国产编程语言蓝皮书-2023》编委会大会会议纪要：  
)

---

### 《国产编程语言蓝皮书-2023》意见征集稿
[https://gitee.com/ploc-org/CNPL-2023/blob/master/pre.md](https://gitee.com/ploc-org/CNPL-2023/blob/master/pre.md)